/*
Physics Fun Badge Project
Read, record, display, output
Acceleration, Distance, and Force
*/
#include <stdlib.h>
#include "simpletools.h"                     // Include simpletools library
#include "ping.h"                            // Include ping header
#include "badgetools.h"                      // Include badgetools library


int x, y, z, cmDist, DataRecs;               // accelerometer axis variables
volatile unsigned int millis, prevCNTms;     // timekeeping variables
volatile float force;                        // FlexiForce & PING))) distance sensor variables

volatile int eei2cLock;

char btn15[], btn16[], btn17[],              // arrays to hold images for menu selections
     btn25[], btn26[], btn27[];

int lastX, lastY;                            // holds last graphed position for drawing lines

int choices, states, newState;               // variable to hold state, determine if state is new
                                             // set newState to 0 before changing states, 
                                             // set newstate to 1 or higher immediately in the state

int ee_ReadInt();
void ee_WriteInt();

void readSensors();                          // forward declaration of functions
void storeSensors();
void readForce();                            // reading force takes time, so it is managed in a
                                             // separate cog

void main()                                  // Main function
{
  badge_setup();                             // Call badge setup
  newState = 0;
  states = 0;

  cog_run(readForce, 64);

  while(1)                                   // Main loop
  {
        
    /*
     *States to chose from:
     -----------------------------
     *00 - Main Menu
     
     *10 - Distance Submenu
     *11 - Distance Display
     *12 - Distance ReadySetGo
     *13 - Distance Record
     *14 - Distance Graph Position
     *15 - Distance Graph Speed
     
     *20 - Accelerometer Submenu
     *21 - Accel Display
     *22 - Accel ReadySetGo
     *23 - Accel Record
     *24 - Accel Graph X
     *25 - Accel Graph Y
     *26 - Accel Graph Z
     
     *30 - Force Submenu
     *31 - Force Display
     *32 - Force ReadySetGo
     *33 - Force Record
     *34 - Force Graph
     
     *40 - All Submenu
     *41 - All Display
     *42 - All ReadySetGo
     *43 - All Record
     
     *99 - Send to terminal
     -----------------------------
    */

    if(newState == 0) 
    {
      rgbs(OFF, OFF);        // Turn off LEDs if the badge transitions to a new state
    //  clear();
      pause(250);
    }      
    
    if(states == 0)       // State 0: Main Menu
    {
      clear();
      pause(100);
      text_size(LARGE);
      oledprint("Physics\n  Fun!  ");
      pause(500);
      clear();
      text_size(SMALL);
      oledprint("--Physics Fun!--   Distance\n   Accel\n   Force\n\n    Send Data            All");
      shape(btn17, SCR_WHITE,   0,  7, 13, 8);
      shape(btn16, SCR_WHITE,   0, 15, 13, 8);
      shape(btn15, SCR_WHITE,   0, 23, 13, 8);
      shape(btn26, SCR_WHITE, 111, 39, 13, 8);         
      shape(btn25, SCR_WHITE, 111, 47, 13, 8);  
      screen_update();
      newState++;
      cursor(0,0);
      oledprint("--");  

      while(buttons() == 0b0000000);
      clear();
      choices = buttons();
      pause(250);
      if(choices == 0b0100000) states = 10;
      if(choices == 0b0010000) states = 20;
      if(choices == 0b0001000) states = 30;
      if(choices == 0b0000100) states = 40;
      if(choices == 0b0000010) states = 99;
    }
                          
    if(states == 10 || states == 20 || states == 30 || states == 40)  // States 10, 20, 30, & 40: Submenus
    {
      clear();
      text_size(SMALL);
      if(states == 10) 
      {
        oledprint("----Distance----");
        cursor(0,3);
        oledprint("   Graph Dist      Graph Spd");
        shape(btn26, SCR_WHITE, 111, 31, 13, 8); 
        shape(btn27, SCR_WHITE, 111, 23, 13, 8);        
      }          
      if(states == 20) 
      {
        oledprint("--Accelerometer-");
        cursor(0,3);
        oledprint("      Graph X         Graph Y         Graph Z");
        shape(btn26, SCR_WHITE, 111, 31, 13, 8);         
        shape(btn25, SCR_WHITE, 111, 39, 13, 8);  
        shape(btn27, SCR_WHITE, 111, 23, 13, 8);                
      }          
      if(states == 30) 
      {
        oledprint("------Force-----");
        cursor(0,3);
        oledprint("        Graph");
        shape(btn27, SCR_WHITE, 111, 23, 13, 8);          
      }          
      if(states == 40) oledprint("-------All------");

      cursor(0,1);
      oledprint("   Display\n   Record");
      cursor(0,6);
      oledprint("   Back");
      shape(btn17, SCR_WHITE,   0,  7, 13, 8);
      shape(btn16, SCR_WHITE,   0, 15, 13, 8);
      shape(btn15, SCR_WHITE,   0, 47, 13, 8);

      screen_update();
      pause(100);

      while(buttons() == 0b0000000);
            
      choices = buttons();
      clear();
      pause(250);
      newState = 0;
      if(choices == 0b0100000) states++;
      if(choices == 0b0010000) states = states + 2;
      if(choices == 0b0001000) states = 0;
      if(choices == 0b0000100 && states == 20) states = states + 6;
      if(choices == 0b0000010 && (states == 10 || states == 20)) states = states + 5;
      if(choices == 0b0000001 && states != 40) states = states + 4;
    }
                 
    if(states == 11 || states == 21 || states == 31 || states == 41)  // States 11, 21, 31, & 41: Display the sensor's current readings
    {
      if(newState == 0)
      {
        clear();
        text_size(SMALL);
        if(states == 11) oledprint("----Distance----");
        if(states == 21) oledprint("---Accel (cg)---");
        if(states == 31) oledprint("------Force-----");
        if(states == 41) oledprint("-------All------");
        oledprint("   Exit");
        shape(btn15, SCR_WHITE,   0, 7, 13, 8);
        screen_update();
        newState++;
        rgbs(BLUE, BLUE);
      }        
      
      if(states == 11 || states == 31)
      {
        cursor(0, 1);
        text_size(LARGE);
      } else 
      {
        text_size(SMALL);
      }          
      if(states == 11) oledprint("%d cm  ", cmDist/58);
      if(states == 21)
      {
        cursor( 0, 4); oledprint("accel x = %03d ", x);
        cursor( 0, 5); oledprint("accel y = %03d ", y);
        cursor( 0, 6); oledprint("accel z = %03d ", z);
      }          
      if(states == 31) oledprint("%.1f N ", 360000/force-1.44);
      if(states == 41)
      {
        cursor( 0, 3); oledprint("Dist  = %d cm  ", cmDist/58);
        cursor( 0, 4); oledprint("Force = %.2f N  ", 360000/force-1.44);
        cursor( 0, 6); oledprint(" x     y     z");
        cursor( 0, 7); oledprint("%03d ", x);
        cursor( 6, 7); oledprint("%03d ", y);
        cursor(12, 7); oledprint("%03d ", z);
      }
      pause(10);
      
      if(buttons() == 0b0001000)
      {
        states = states - 1;
        rgbs(OFF, OFF);        
        clear();
        pause(250); 
      }                      
    } 
            
    if(states == 12 || states == 22 || states == 32 || states == 42)  // States 12, 22, 32, & 42: Ready Set Record Sequence (immediately transitions to the next state when finished)
    {
      clear();
      text_size(SMALL);
      oledprint("Press any button\nto begin\nrecording");
      
      while(buttons() == 0b0000000);

      clear();
      pause(500);     
      text_size(LARGE);
      rgbs(RED, RED);
      oledprint("Ready...");
      pause(500);
      rgbs(OFF, OFF);
      pause(500);
      rgbs(YELLOW, YELLOW);
      clear();
      oledprint("Set...");
      pause(500);
      rgbs(OFF, OFF);
      pause(500);
      
      states++;
      newState = 0;
    } 

    if(buttons() == 0b0000000 && (states == 13 || states == 23 || states == 33 || states == 43))  // States 13, 23, 33, & 43: Recording sensor readings to EEPROM
    {
      if(newState == 0)
      {
        clear();
        rgbs(GREEN, GREEN);
        text_size(LARGE);
        oledprint("Record\n");
        text_size(SMALL);
        cursor(0,4);
        oledprint("Press any buttonto stop");
        millis = 0;
        newState++;
        DataRecs = 35;
        pause(20);
      } else 
      {           
        newState++;              
        
        // Record zeros into sensors that are not being recorded to speed things up.

        if(states == 13) {storeSensors(cmDist,0,0,0,0); cursor(0,6); oledprint("%d samples \n%d cm  ", newState,cmDist/58);}
        if(states == 23) {storeSensors(0,x,y,z,0); cursor(0,6); oledprint("%d samples \n%03d %03d %03d  ", newState,x,y,z);}
        if(states == 33) {storeSensors(0,0,0,0,(int) force); cursor(0,6); oledprint("%d samples\n%.2f N  ", newState, 360000/force-1.44);}
        if(states == 43) {storeSensors(cmDist,x,y,z,(int) force); cursor(0,6); oledprint("\n%d samples", newState);}
      }          

    } else if(buttons() != 0b0000000 && (states == 13 || states == 23 || states == 33 || states == 43))  // Detect if done recording
    { 
      ee_WriteInt(DataRecs, 30 + 32768);      // Record the number of readings recorded in EEPROM into address 30 + 32768
      clear();
      pause(250);
      newState = 0;
      states = 0;              
    }  
        
    readSensors(); 
         
    if(states == 99)  // State 99: Output data recorded to EEPROM to SimpleIDE Terminal.  Comma-Separated for paste into MS Excel/Spreadsheets.
    {
      clear();
      text_size(SMALL);
      oledprint("Connect to\nSimpleIDE\nTerminal\n\nPress any buttonto send");
      while(buttons() == 0b0000000);       // wait for a button to be pressed
      clear(); 
      pause(500);      

      rgbs(RED, RED); 
      text_size(LARGE);
      oledprint("Sending   Data");
      print("millisecond,distance,accel-x,accel-y,accel-z,force\n"); 
      DataRecs = (ee_ReadInt(30 + 32768)-34)/25;
      for(int record = 0; record < DataRecs; record++)
      {
        int d1 = ee_ReadInt(record*25 + 35 + 32768);
        int d2 = ee_ReadInt(record*25 + 39 + 32768);
        int d3 = ee_ReadInt(record*25 + 43 + 32768);
        int d4 = ee_ReadInt(record*25 + 47 + 32768);
        int d5 = ee_ReadInt(record*25 + 51 + 32768);
        int d6 = ee_ReadInt(record*25 + 55 + 32768);
        
        print("%d,%d,%d,%d,%d,%d\n", d1 - ee_ReadInt(35 + 32768), d2, d3, d4, d5, d6);
      } 
      clear(); 
      rgbs(OFF, OFF); 
      newState = 0;
      states = 0;              
    }    
           
    if(states == 14 || states == 34)  // States 14 & 34: Graphing distance and force.  Combined because the y axis is at the bottom of the screen.
    {
      clear();
      text_size(SMALL);
      DataRecs = (ee_ReadInt(30 + 32768) - 25)/25 - 1; 
             
      cursor(0,0);
      if(states == 14)
      {
        oledprint("315 cm  - Dist");
        cursor(0,7);
        oledprint("0cm");
      } else
      {
        oledprint("120 N  - Force");
        cursor(0,7);
        oledprint("0 N");
      }
      oledprint("/0s      %ds", (ee_ReadInt((DataRecs)*25 + 35 + 32768) - ee_ReadInt(35 + 32768))/1000 );
      
      line(0, 10, 0, 54, 1);                     // Line along left of screen
      line(0, 54, 128, 54, 1);                   // Horizontal line across center      

      if(DataRecs > 128)
      {
        float scalar = (DataRecs-1)/128;
        for(int x = 0; x < 128; x = x + scalar)  // Count 0 to 127
        {
          if(states == 14)
          {
            y = 54-((int) ee_ReadInt(x*25 + 39 + 32768))/420;
          } else 
          {
            y = (int) ee_ReadInt(x*25 + 55 + 32768);
            y = 54-(360000/y-1.44)/2.73;                
          }
          if(x == 0)
          {                                
            point(x, y, 1);                      // Plot white point
          } else 
          {
            line(lastX, lastY, x, y, 1);
          }
          lastX = x; lastY = y;               
        }   
      } else 
      {
        float scalar = 128/(DataRecs-1);
        for(int x = 0; x < DataRecs; x++)        // Count 0 to 127
        {
          if(states == 14)
          {
            y = 54-((int)ee_ReadInt(x*25 + 39 + 32768))/420;
          } else 
          {
            y = (int)ee_ReadInt(x*25 + 55 + 32768);
            y = 54-(360000/y-1.44)/2.73; 
           
          }                                
          if(x == 0)
          {                                
            point(x, y, 1);                      // Plot white point
          } else 
          {
            line(lastX, lastY, x, y, 1);
          }
          lastX = x; lastY = y;               
        }   
      }
      
      while(buttons() == 0b0000000);
      clear(); 
      pause(250);
      states = (states/10)*10;                                                  
    }        

    if(states == 24 || states == 25 || states == 26)  // States 24, 25, & 26: Graphing Accelerometer Tilt angle (X, Y, and Z).
    {
      clear();
      text_size(SMALL);
      cursor(0,0);
      oledprint("1.5g -  accel %c", (char) (states + 64));
      cursor(0,7);
      DataRecs = (ee_ReadInt(30 + 32768) - 25)/25 - 1; 
      oledprint("-1.5g/0s   %ds", (ee_ReadInt((DataRecs)*25+35 + 32768) - ee_ReadInt(35 + 32768))/1000 );
      
      line(0, 10, 0, 54, 1);                      // Line along left of screen
      line(0, 32, 128, 32, 1);                    // Horizontal line across center

      if(DataRecs > 128)
      {
        float scalar = (DataRecs-1)/128;
        for(int x = 0; x < 128; x = x + scalar)   // Count 0 to 127
        {
          y = 32-((int)ee_ReadInt(x*25 + (states-24)*4 + 32811))/6.9;
          if(x == 0)
          {                                
            point(x, y, 1);                       // Plot white point
          } else 
          {
            line(lastX, lastY, x, y, 1);
          }
          lastX = x; lastY = y;               
        }   
      } else 
      {
        float scalar = 128/(DataRecs-1);
        for(int x = 0; x < DataRecs; x++)         // Count 0 to 127
        {
          y = 32-((int)ee_ReadInt(x*25 + (states-24)*4 + 32811))/6.9;
          if(x == 0)
          {                                
            point(x, y, 1);                       // Plot white point
          } else 
          {
            line(lastX, lastY, x, y, 1);
          }
          lastX = x; lastY = y;               
        }   
      }
      
      while(buttons() == 0b0000000);
      clear(); pause(250);
      states = (states/10)*10;
    }        

    if(states == 15)  // State 15: Graphing Speed - done by taking the derivative of Distance.
    {
      clear();
      text_size(SMALL);
      DataRecs = (ee_ReadInt(30 + 32768) - 25)/25 - 1; 
      cursor(0,0);
      oledprint("2 mps  - Speed");
      cursor(0,7);
      oledprint("-2mps/0s  %ds", (ee_ReadInt((DataRecs)*25 + 35 + 32768) - ee_ReadInt(35 + 32768))/1000 );
      
      line(0, 10, 0, 54, 1);                     // Line along left of screen
      line(0, 32, 128, 32, 1);                   // Horizontal line across center

      if(DataRecs > 128)
      {
        float scalar = (DataRecs-1)/128;
        for(int x = scalar; x < 128; x = x + scalar) // Count 0 to 127
        {
          y = (ee_ReadInt(x*25 + 32807) - ee_ReadInt(x*25 + 32782))*2/(ee_ReadInt(x*25 + 32803) - ee_ReadInt(x*25 + 32778));
          y = 32 - y;
          if(x == 0)
          {                                
            point(x, y, 1);                          // Plot white point
          } else 
          {
            line(lastX, lastY, x, y, 1);
          }
          lastX = x; lastY = y;               
        }   
      } else 
      {
        float scalar = 128/(DataRecs-1);
        for(int x = 1; x < DataRecs; x++)            // Count 0 to 127
        {
          y = (ee_ReadInt(x*25 + 32807) - ee_ReadInt(x*25 + 32782))*2/(ee_ReadInt(x*25 + 32803) - ee_ReadInt(x*25 + 32778));
          y = 32 - y;
          if(x == 0)
          {                                
            point(x, y, 1);                          // Plot white point
          } else 
          {
            line(lastX, lastY, x, y, 1);
          }
          lastX = x; lastY = y;               
        }   
      }
      
      while(buttons() == 0b0000000);
      clear(); 
      pause(250);
      states = (states/10)*10;
    }                                                   
  }    
}


void readSensors() 
{
  if(states/10 == 1 || states/10 == 4) cmDist = ping(11);  // Get cm distance from Ping))) if it is in a state that needs it

  if(states/10 == 2 || states/10 == 4)                     // Get accelerometer values if it is in a state that needs it
  {
    x = accel(AX);                           
    y = accel(AY);
    z = accel(AZ);
  }    

} 

void readForce()                                           // Read Flexiforce sensor - done in it's own cog, because it takes a significant amount of time to do.
{
  while(1)
  {
      high(10);
      pause(1);
      force = rc_time(10, 1);
  }       
}

void storeSensors(int s1, int s2, int s3, int s4, int s5)  // Write sensor values (or zeros) to EEPROM.
{
  if(DataRecs <= 65485)
  {
    prevCNTms = ((unsigned int) CNT)/((unsigned int) ms) + (millis * 53687); // Timekeeping.  Keep track of the system clock in milliseconds.  Account for rollovers.
    ee_WriteInt(prevCNTms, DataRecs + 32768);
    if(DataRecs != 35 && prevCNTms < ee_ReadInt(DataRecs + 32768 - 25))       // If the clock has rolled over, add to the variable that accounts for that.
    {
      millis++; 
      prevCNTms = ((unsigned int) CNT)/((unsigned int) ms) + (millis * 53687);
      ee_WriteInt(prevCNTms, DataRecs + 32768);
    }          
    ee_WriteInt(s1, DataRecs + 4  + 32768);                     // Record sensor readings.
    ee_WriteInt(s2, DataRecs + 8  + 32768);
    ee_WriteInt(s3, DataRecs + 12 + 32768);
    ee_WriteInt(s4, DataRecs + 16 + 32768);
    ee_WriteInt(s5, DataRecs + 20 + 32768);
    DataRecs += 25;                                          // Set EEPROM address 30 + 32768 to the address of the last reacorded readings.
  } else 
  {
    ee_WriteInt(DataRecs, 30 + 32768);      // Record the number of readings recorded in EEPROM into address 30 + 32768
    clear();
    text_size(LARGE);
    oledprint(" Memory   FULL");
    rgbs(RED, RED);
    states = 0;
    pause(1500);
  }
} 

void ee_WriteInt(int EEdata, int EEaddr)
{
  while(lockset(eei2cLock));
  ee_putInt(EEdata, EEaddr);
  lockclr(eei2cLock);  
  return;
}  

int ee_ReadInt(int EEaddr)
{
  while(lockset(eei2cLock));
  int value = ee_getInt(EEaddr);
  lockclr(eei2cLock); 
  return value;   
}  

char btn15[] =                                             // Define menu images.  These appear as black numbers on a white background when XOR'd.
{
0b11100110,
0b00001110,
0b00110000,
0b01111001,
0b10011111,
0b11001100,
0b00111110,
0b01111000,
0b11110011,
0b11100111,
0b00001000,
0b00111000,
0b01000011
};

char btn16[] =
{
0b11100111,
0b00011110,
0b00110000,
0b01111001,
0b10011111,
0b11001100,
0b00111110,
0b01100000,
0b11110011,
0b00100111,
0b00001000,
0b00111000,
0b01100011
};

char btn17[] =
{
0b11100110,
0b00001110,
0b00110000,
0b01111001,
0b11110111,
0b11001111,
0b00111110,
0b01110011,
0b11110011,
0b10011111,
0b00001100,
0b11111000,
0b01100111
};

char btn25[] =
{
0b11000110,
0b00001100,
0b00010000,
0b01100100,
0b10011111,
0b11001100,
0b00111100,
0b11111000,
0b11001111,
0b11100110,
0b00001000,
0b00110000,
0b01000011
};

char btn26[] =
{
0b11000111,
0b00011100,
0b00010000,
0b01100100,
0b10011111,
0b11001100,
0b00111100,
0b11100000,
0b11001111,
0b00100110,
0b00001000,
0b00110000,
0b01100011
};

char btn27[] =
{
0b11000110,
0b00001100,
0b00010000,
0b01100100,
0b11110111,
0b11001111,
0b00111100,
0b11110011,
0b11001111,
0b10011110,
0b00001100,
0b11110000,
0b01100111
};
void SaisirEtudient(String nom,String prenom,int id,)
{ 
oledprint("saisir le nom d'etudient"); 
text_size(SMALL);                         
  cursor(0, 5);
scanf("%s", &nom);
oledprint("saisir le prenom"); 
text_size(SMALL);                         
  
scanf("%s", &prenom);
oledprint("saisir le CIN"); 
text_size(SMALL); 
 scanf("%d", &id);
oledprint("cours compris"); 
text_size(SMALL); 

 oledprint("Séance "); 
text_size(SMALL); 
 scanf("%s", &seance );
   
}

bool  avoirdifficulte()
{
	return true;
	
}
void ComprendreCours()
{if(avoivfficule)
	{oledprint("ON A trouver quelq'un qui comprend le cours");}
}


bool gestiondacces(bool tag,bool acces)
{if(tag==true)
	{if(acces==true)
		{oledprint("Bonjour, bienvenue  ");}
	else{oledprint("Au revoir !");
	}
		}
		else{ oledprint("acces non autorisee")
			
		}
	
}
void randomAction()
{char r;
	oledprint("Voulez vous faire un defi? si oui repond avec O sinon N  ");
scanf("%c", &r );
if(r=='O')
{int i,n;

i=rand_a_b(1,n);
switch (i)
​{
    case 1
     defiObese()
      break;
    case 2
      defiChanter()
      break;
  
    default:
      // default statements
}


}
else if(r=='N')
	
	{oledprint("ops! votre prochaine souhait va etre rejeté  ");}


	
}
void defiObese()
{x=readSensors();
	oledprint("la salle que vous voulez attient est de cordonnées  ");

randpostionsalle(String position)
pause(5000);
 y=readSensors();
 if(x!=y)
 {oledprint("vous avez  bien fait une bon sport Bravo vous avez perdu  de calorie quand meme  ");}
else{
	oledprint("Mais vous etes tres paresseu  ");

	
}

	
}

void randpostionsalle(String position)
{random(position);
	
}
void defiChanter()
{String c=choisiramiChanson;
byte[] voice;
	oledprint("Salut vous etiez choisi ,  Pour chanter la cahnson que VOTRE AMI l'a choisi %s",c);
	getVoice(voice);
	boradcast(voice);
}
String choisiramiChanson()
{
	String c;
	oledprint("choisir une chanson :D "); 
text_size(SMALL);                         
  cursor(0, 5);
scanf("%s", &c);
return c;

	
}
Byte [] getVoice(byte voice)
{oledprint("chantez le micro est entrain d'enregistrer");
wait(5000);
return voice;
	
}
Byte [] boradcast(byte voice)
{oledprint("tous le monde va ecouter Votre voix ");
wait(5000);

	
}
int rand_a_b(int a, int b){
    return rand()%(b-a) +a;-
}
void alerteEnseignatAbsent()
{oledprint("le professeu est absent");

	
}
void seanceDerattrappage()
{oledprint("vous avez une seance de rattrapage");
	
}
void alerteDevoir()
{oledprint("vous avez un devoir");
	
}




